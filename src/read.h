#pragma once

#include <stdbool.h>

#define BUFFER_SIZE 65536 //zone mémoire disponible

typedef struct _dataBufferReader {
    int fd; //un file descriptor
    unsigned int pos, size, posBit; 
    char *buffer; 
} _dataBufferReader; 

//initialise une structure de donnée de type dataBufferReader et la retourne.
dataBufferReader *createBufferReader(char *filename); 

//ferme le fichier et libére la mémoire alloué lors de la création de la structure.
void destroyBufferReader(dataBufferReader *data); 

//récupére le prochain caractère.
int getCurrentChar(dataBufferReader *data); 

//consomme le prochain caractère.
void consumeChar(dataBufferReader *data); 

//vérifie si nous n'avons pas atteint la fin du fichier.
bool eof(dataBufferReader *data); 

//retourne au début du fichier.
void moveBeginning(dataBufferReader *data); 

//récupére le prochain bit (on suppose une lecture bit par bit).
bool getCurrentBit(dataBufferReader *data); 

//consomme le prochain bit (on suppose une lecture bit par bit).
void consumeBit(dataBufferReader *data); 